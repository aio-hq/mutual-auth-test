// var http = require('http');
 
// if (process.argv.length <= 2) {
//     console.log("Usage: " + __filename + " URL");
//     process.exit(-1);
// }
 
// var url = process.argv[2]
 
// http.get(url, function(res) {
//     console.log("Got response: " + res.statusCode);
// }).on('error', function(e) {
//     console.log("Got error: " + e.message);
// });


const https = require('https');
fs = require('fs');

var options = {
  hostname: 'localhost',
  port: 8443,
  path: '/Mentored-Research/api/status',
  method: 'GET',
  key: fs.readFileSync('/Applications/apache-tomcat-8.0.28/cert-test/client-cert/user.key'),
  cert: fs.readFileSync('/Applications/apache-tomcat-8.0.28/cert-test/client-cert/user.crt'),
  passphrase: 'password',
  rejectUnauthorized: false,
  requestCert: true,
  agent: false
};

var req = https.request(options, function(res) {
  console.log('statusCode:', res.statusCode);
  console.log('headers:', res.headers);

  res.on('data', function(res) {
    console.log("The data is here!!");
    console.log(res);
  });

  res.on('error', function(res) {
    console.log("The error is here!!");
    console.log(res);
  });

});

req.end();

